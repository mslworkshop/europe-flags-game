// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('activeAnimation', [])
.directive('activeAnimation', ['$rootScope', function ($rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, iAttrs) {
            $rootScope.$on('updatetime', function(event, newWidth){
                element.css({
                    width: newWidth,
                    background: '#669900'
                });
            });
        }
    };
}]);
angular.module('MSLEuropeFlagsGame', [
    'ionic',
    'activeAnimation'
])
.config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {

    $stateProvider.state('index', {
        url: "/",
        templateUrl: "tpls/main.tpl.html"
    })
    .state('play', {
        url: "/play",
        templateUrl: "tpls/play.tpl.html",
        controller: 'PlayCtrl'
    })
    .state('scores', {
        url: "/scores",
        templateUrl: "tpls/scores.tpl.html",
        controller: 'ScoresCtrl'
    })
    .state('settings', {
        url: "/settings",
        templateUrl: "tpls/settings.tpl.html",
        controller: 'SettingsCtrl'
    });
    $urlRouterProvider.otherwise("/");
}])
.run(function($ionicPlatform, $rootScope) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        /*if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }*/
        if (adbuddiz) {
            adbuddiz.setAndroidPublisherKey("197f4ca9-ffd8-466e-82e2-5e2937323465");
            adbuddiz.cacheAds();
        }
        /*if(window.AdMob) {
            // var admob_key = $ionicPlatform.isAndroid() ? "pub-7110968239218267" : "IOS_PUBLISHER_KEY";
            var AdMob = window.AdMob;
            AdMob.createBanner({
                adId: "pub-7110968239218267",
                adSize: AdMob.AD_SIZE.BANNER,
                position: AdMob.AD_POSITION.BOTTOM_CENTER,
                autoShow: true
            }, function(){
            }, function(){
            });
        }*/
        cordova.getAppVersion(function(version) {
            $rootScope.$apply(function(){
                $rootScope.appVersion = version;
            });
        });
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
    
})
.factory('$localStorage', ['$window', function($window) {
    return {
        set: function(key, value) {
            $window.localStorage[key] = value;
        },
        get: function(key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        setObject: function(key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function(key) {
            return JSON.parse($window.localStorage[key] || '{}');
        }
    };
}])
.service('User', ['$localStorage', function($localStorage){
    var isFirstTime = $localStorage.get('isFirstTime'),
        userData = $localStorage.getObject('user'),
        userSettings = $localStorage.getObject('settings');

    function getLevel(){
        return parseInt(userSettings.level, 10);
    }
    function getUserName(){
        return userData.username;
    }
    function getMaxPoints(){
        return userData.maxPoints;
    }
    function getTotalGames(){
        return userData.totalGames;
    }
    function getIsFirstTime(){
        var output = isFirstTime == 'false' ? false : true;
        return output;
    }
    function setDefaults(){
        var isFirstTime = false,
            userInfo = {
                maxPoints: 0,
                totalGames: 0,
                username: 'Guest'
            },
            defaultSettings = {
                level: 1
            };
        $localStorage.set('isFirstTime', isFirstTime);
        $localStorage.setObject('settings', defaultSettings);
        $localStorage.setObject('user', userInfo);
        refreshData();
    }
    function updateScores(newMaxPoints){
        var totalGames = getTotalGames(),
            newScores = {
                maxPoints: getMaxPoints(),
                totalGames: (++totalGames),
                username: getUserName()
            };
        if(newMaxPoints !== false){
            newScores.maxPoints = newMaxPoints;
        }
        $localStorage.setObject('user', newScores);
        refreshData();
    }
    function updateSettings(newSettings){
        $localStorage.setObject('settings', newSettings);
        refreshData();
    }
    function refreshData(){
        isFirstTime = $localStorage.get('isFirstTime');
        userData = $localStorage.getObject('user');
        userSettings = $localStorage.getObject('settings');
    }
    return {
        getLevel: getLevel,
        getIsFirstTime: getIsFirstTime,
        setDefaults: setDefaults,
        updateScores: updateScores,
        getMaxPoints: getMaxPoints,
        getTotalGames: getTotalGames,
        updateSettings: updateSettings
    };
}])
.filter('seconds', [function(){
    return function(input){
        var seconds = input / 1000;
        if(seconds % 1 === 0){
            seconds += '.0';
        }
        return seconds + 's';
    };
}])
.service('Game', ['$localStorage', 'User', function ($localStorage, User) {
    var cfg = {
        isGameActive: false
    };
    function getRandomCountries(number, currentCountry, countries){
        var randomCountries = [];
        while(randomCountries.length < number){
            var randomItem = countries[Math.floor(Math.random() * (countries.length - 1))];
            if ((currentCountry !== randomItem.name) && (randomCountries.indexOf(randomItem.name) === -1)) {
                randomCountries.push(randomItem.name);
            }
        }
        return randomCountries;
    }
    function shuffle(o){ //v1.0
        for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        return o;
    }
    function startGame(countries, answersNum){
        var questions = [];

        shuffle(countries);
        angular.forEach(countries, function(country){
            var output = {
                correct: country.name,
                flag: country.flag,
                answers: getRandomCountries(answersNum, country.name, countries)
            };
            output.answers.push(country.name);
            shuffle(output.answers);
            questions.push(output);
        });
        return questions;
    }
    function getActiveGame(){
        return cfg.isGameActive;
    }
    function setActiveGame(isActive){
        cfg.isGameActive = isActive;
        return cfg.isGameActive;
    }
    function calculatePoints(isCorrect){
        var points = 0,
            level = User.getLevel();
        if (isCorrect === true) {
            switch(level){
                case 1:
                    points = 10;
                    break;
                case 2:
                    points = 10;
                    break;
                case 3:
                    points = 10;
                    break;
                default:
                    points = 0;
                    break;
            }
        }
        else if(level === 3){
            points = -5;
        }
        return points;
    }
    function calculateBonus(correctInRow){
        var minCorrectInRow = 3;
        if (User.getLevel() === 3) {
            minCorrectInRow = 5;
        }
        if (correctInRow > minCorrectInRow) {
            if (User.getLevel() === 1) {
                return 10;
            }
            else{
                return 5;
            }
        }
        return 0;
    }
    function calculateAddTime(correctInRow){
        var plusTime = 0,
            level = User.getLevel();
        switch(level){
            case 1:
                plusTime = 4000;
                break;
            case 2:
                plusTime = 3000;
                break;
            case 3:
                plusTime = 2500;
                break;
            default:
                plusTime = 0;
                break;
        }
        if (correctInRow > 85 && level === 1) {
            plusTime -= 2500;
        }
        else if (correctInRow > 75) {
            plusTime -= 2000;
        }
        else if (correctInRow > 50) {
            plusTime -= 1500;
        }
        else if(correctInRow > 30){
            plusTime -= 1000;
        }
        else if(correctInRow > 15){
            plusTime -= 500;
        }
        return plusTime;
    }
    function getMaxWrong(){
        switch(User.getLevel()){
            case 1:
                return 15;
            case 2:
                return 10;
            case 3:
                return 5;
            default:
                return 0;
        }        
    }
    return {
        getActiveGame: getActiveGame,
        setActiveGame: setActiveGame,
        startGame: startGame,
        calculatePoints: calculatePoints,
        calculateBonus: calculateBonus,
        calculateAddTime: calculateAddTime,
        getMaxWrong: getMaxWrong
    };
}])
.controller('MainCtrl', ['$scope', '$rootScope', '$state', '$log', '$ionicPopup', '$ionicPlatform', 'User', 'Game', function($scope, $rootScope, $state, $log, $ionicPopup, $ionicPlatform, User, Game){
    $scope.cfg = {
        isMenuVisible: false
    };
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        $rootScope.previousState = fromState.name;
        $rootScope.currentState = toState.name;
        if(toState.name === 'play'){
            toParams.isGoBackAccepted = false;
        }
        if ($rootScope.currentState === '' || $rootScope.currentState === 'index') {
            $scope.cfg.isMenuVisible = false;
        }
        else{
            $scope.cfg.isMenuVisible = true;
        }
    });
    $scope.goBack = function(){
        $log.info('goBack', $rootScope.currentState, Game);
        if($rootScope.currentState === 'play' && Game.getActiveGame() === true){
            var confirmPopup = $ionicPopup.confirm({
                title: 'Quit game',
                template: 'You will loose your progress. Are you sure?',
                okText: 'Yes'
            });
            confirmPopup.then(function(isPositive) {
                if(isPositive) {
                    $state.go('index');
                }
            });
        }
        else{
            $state.go('index');
        }
    };
    $ionicPlatform.registerBackButtonAction(function () {
        if ($rootScope.currentState === '' || $rootScope.currentState === 'index') {
            navigator.app.exitApp();
        }
        else {
            $scope.goBack();
        }
    }, 100);
    if (User.getIsFirstTime() !== false) {
        User.setDefaults();
    }
}])
.controller('PlayCtrl', ['$scope', '$state', '$log', '$rootScope', '$interval', '$ionicPopup', '$localStorage', 'Countries', 'Game', 'User', function($scope, $state, $log, $rootScope, $interval, $ionicPopup, $localStorage, Countries, Game, User){
    var level = User.getLevel(),
        config = {
            defaultTotalTime: 10000,
            totalTime: 10000,
            elapsedTime: 0,
            isActiveInterval: false,
            intervalOffset: 200,
            answersNumber: level == 1 ? 3 : 5,
            maxTimeSpan: 30000
        };
    $scope.cfg = {
        ready: false,
        totalTime: config.totalTime
    };
    $scope.$on('$destroy', function() {
        $interval.cancel(config.isActiveInterval);
        config.isActiveInterval = undefined;
        if (!($scope.data.correct === 0 && $scope.data.wrong === 0)) {
            User.updateScores(false);
        }
        $scope.data = {};
        $scope.cfg = {};
        config = {};
    });
    adbuddiz.showAd();
    function endGame(){
        $interval.cancel(config.isActiveInterval);
        config.isActiveInterval = undefined;
        $scope.data.isActiveGame = false;
        Game.setActiveGame(false);
        if (angular.isUndefined(User.getMaxPoints()) || (User.getMaxPoints() < $scope.data.totalPoints)) {
            if (User.getMaxPoints() < $scope.data.totalPoints) {
                $ionicPopup.alert({
                    title: 'Congratulations!',
                    template: 'You achieved new highest score!',
                    okText: 'Close'
                });
            }
            User.updateScores($scope.data.totalPoints);
        }
    }
    $scope.startGame = function(){
        $scope.data = {
            questions: Game.startGame(Countries, config.answersNumber),
            activeQuestion: 0,
            correct: 0,
            wrong: 0,
            remainingWrong: Game.getMaxWrong(),
            totalPoints: 0,
            totalGames: User.getTotalGames(),
            isActiveGame: true,
            correctInRow: 0,
            bonus: 0,
            totalTime: config.totalTime,
            elapsedTime: 0
        };
        $scope.cfg.ready = true;
        config.elapsedTime = 0;
        config.totalTime = config.defaultTotalTime;
        $scope.data.totalTime = config.totalTime;
        $scope.cfg.totalTime = config.totalTime;
        Game.setActiveGame(true);
        $rootScope.$emit('updatetime', 0);
        config.isActiveInterval = $interval(function(){
            $log.info('Interval');
            config.elapsedTime += config.intervalOffset;
            var elapsedTime = config.elapsedTime;

            if (elapsedTime > config.totalTime) {
                $log.error('End game!');
                $scope.data.elapsedTime = config.totalTime;
                endGame();
            }
            else{
                $scope.data.elapsedTime = config.elapsedTime;
                var newWidth = ((config.elapsedTime / $scope.data.totalTime) * 100) + '%';
                $rootScope.$emit('updatetime', newWidth);
            }
        }, config.intervalOffset);

    };
    var areYouReady = $ionicPopup.alert({
        title: 'Are you ready?',
        template: 'Let\'s go!',
        okText: 'Start'
    });
    areYouReady.then(function(res) {
        $scope.startGame();
    });
    function isGameStarted(){
        return $scope.data.isActiveGame;
    }
    function updateTime(plusTime){
        var newTime = false;
        $scope.cfg.totalTime += plusTime;
        newTime = $scope.cfg.totalTime;
        config.totalTime = newTime;
        $scope.data.totalTime = newTime;
    }
    function getRemainingTime(){
        return config.totalTime - config.elapsedTime;
    }
    $scope.selectAnswer = function(answer){
        if (!isGameStarted()) {
            return false;
        }
        var currentQuestion = $scope.data.questions[$scope.data.activeQuestion],
            next = 0,
            bonus = 0,
            pointsFromQuestion = 0,
            isCorrect = (currentQuestion.correct === answer);

        if (isCorrect === true) {
            ++$scope.data.correct;
            ++$scope.data.correctInRow;

            bonus = Game.calculateBonus($scope.data.correctInRow);
            if (getRemainingTime() < config.maxTimeSpan) {
                updateTime(Game.calculateAddTime($scope.data.correctInRow));
            }
            else{
                bonus += 5;
            }
            $scope.data.bonus = bonus;
        }
        else{
            ++$scope.data.wrong;
            $scope.data.remainingWrong = Game.getMaxWrong() - $scope.data.wrong;
            $scope.data.correctInRow = 0;
            $scope.data.bonus = 0;
            if ($scope.data.remainingWrong <= 0) {
                endGame();
            }
        }
        pointsFromQuestion = Game.calculatePoints(isCorrect);
        $scope.data.totalPoints += (bonus + pointsFromQuestion);
        if (($scope.data.activeQuestion + 1) < $scope.data.questions.length) {
            next = $scope.data.activeQuestion + 1;
        }
        $scope.data.activeQuestion = next;
    };
    $scope.calculateRatio = function(){
        var total = $scope.data.correct + $scope.data.wrong,
            result = '-';

        if (total !== 0) {
            result = ($scope.data.correct / total).toFixed(2);
        }
        if (isNaN(result)) {
            result = '-';
        }
        return result;
    };
}])
.controller('ScoresCtrl', ['$scope', '$localStorage', 'User', function($scope, $localStorage, User){

    $scope.data = {
        maxScore: User.getMaxPoints(),
        totalGames: User.getTotalGames()
    };
}])
.controller('SettingsCtrl', ['$scope', '$rootScope', '$state', '$localStorage', '$ionicHistory', 'User', function($scope, $rootScope, $state, $localStorage, $ionicHistory, User){
    var cfg = $localStorage.getObject('settings');

    $scope.settings = {
        userLevel: angular.isDefined(cfg.level) ? cfg.level : 2,
    };
    $scope.saveSettings = function(){
        User.updateSettings({
            level: $scope.settings.userLevel
        });
        $state.go('index');
    };
}])




























.constant('Countries', [{
    name: 'Albania',
    flag: 'albania'
}, {
    name: 'Andorra',
    flag: 'andorra'
}, {
    name: 'Armenia',
    flag: 'armenia'
}, {
    name: 'Austria',
    flag: 'austria'
}, {
    name: 'Azerbaijan',
    flag: 'azerbaijan'
}, {
    name: 'Belarus',
    flag: 'belarus'
}, {
    name: 'Belgium',
    flag: 'belgium'
}, {
    name: 'Bosnia and Herzegovina',
    flag: 'bosniaandherzegovina'
}, {
    name: 'Bulgaria',
    flag: 'bulgaria'
}, {
    name: 'Croatia',
    flag: 'croatia'
}, {
    name: 'Cyprus',
    flag: 'cyprus'
}, {
    name: 'Czech Republic',
    flag: 'czechrepublic'
}, {
    name: 'Denmark',
    flag: 'denmark'
}, {
    name: 'Estonia',
    flag: 'estonia'
}, {
    name: 'Finland',
    flag: 'finland'
}, {
    name: 'France',
    flag: 'france'
}, {
    name: 'Georgia',
    flag: 'georgia'
}, {
    name: 'Germany',
    flag: 'germany'
}, {
    name: 'Greece',
    flag: 'greece'
}, {
    name: 'Hungary',
    flag: 'hungary'
}, {
    name: 'Iceland',
    flag: 'iceland'
}, {
    name: 'Ireland',
    flag: 'ireland'
}, {
    name: 'Italy',
    flag: 'italy'
}, {
    name: 'Kazakhstan',
    flag: 'kazakhstan'
}, {
    name: 'Liechtenstein',
    flag: 'liechtenstein'
}, {
    name: 'Lithuania',
    flag: 'lithuania'
}, {
    name: 'Luxembourg',
    flag: 'luxembourg'
}, {
    name: 'Macedonia',
    flag: 'macedonia'
}, {
    name: 'Malta',
    flag: 'malta'
}, {
    name: 'Moldova',
    flag: 'moldova'
}, {
    name: 'Monaco',
    flag: 'monaco'
}, {
    name: 'Montenegro',
    flag: 'montenegro'
}, {
    name: 'Netherlands',
    flag: 'netherlands'
}, {
    name: 'Norway',
    flag: 'norway'
}, {
    name: 'Poland',
    flag: 'poland'
}, {
    name: 'Portugal',
    flag: 'portugal'
}, {
    name: 'Russia',
    flag: 'russia'
}, {
    name: 'San Marino',
    flag: 'sanmarino'
}, {
    name: 'Serbia',
    flag: 'serbia'
}, {
    name: 'Slovakia',
    flag: 'slovakia'
}, {
    name: 'Slovenia',
    flag: 'slovenia'
}, {
    name: 'Spain',
    flag: 'spain'
}, {
    name: 'Sweden',
    flag: 'sweden'
}, {
    name: 'Switzerland',
    flag: 'switzerland'
}, {
    name: 'Turkey',
    flag: 'turkey'
}, {
    name: 'Ukraine',
    flag: 'ukraine'
}, {
    name: 'United Kingdom',
    flag: 'unitedkingdom'
}, {
    name: 'Vatican City',
    flag: 'vaticancity'
}, {
    name: 'Kosovo',
    flag: 'kosovo'
}, {
    name: 'Abkhazia',
    flag: 'abkhazia'
}, {
    name: 'Nagorno-Karabakh',
    flag: 'nagornokarabakh'
}, {
    name: 'Gibraltar',
    flag: 'gibraltar'
}, {
    name: 'Faroe Islands',
    flag: 'faroeislands'
}]);
